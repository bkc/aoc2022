package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	lines := bufio.NewScanner(os.Stdin)

	var score int
	for lines.Scan() {
		line := lines.Text()
		lScore := solveProper(line)
		score += lScore
		fmt.Printf(" Score: %d\n ", lScore)
	}
	fmt.Printf("score: %d\n", score)
}

const (
	Rock    byte = 1
	Paper   byte = 2
	Scissor byte = 3
)

func solve(in string) int {
	them := in[0] - 0x40
	me := in[2] - 0x57

	fmt.Printf("T: %d - M: %d", them, me)

	// Draw
	if them == me {
		return int(me) + 3
	}

	switch me {
	case Rock:
		if them == Scissor {
			return int(me) + 6 // win
		}
	case Paper:
		if them == Rock {
			return int(me) + 6 // win
		}
	case Scissor:
		if them == Paper {
			return int(me) + 6 // win
		}
	}

	// I lost...
	return int(me) + 0
}

const (
	Lose byte = 1
	Draw byte = 2
	Win  byte = 3
)

func va(in, strat byte) byte {
	switch strat {
	case Lose:
		switch in {
		case Rock:
			return Scissor
		case Paper:
			return Rock
		case Scissor:
			return Paper
		}

	case Win:
		switch in {
		case Rock:
			return Paper
		case Paper:
			return Scissor
		case Scissor:
			return Rock
		}
	}

	return in
}

func solveProper(in string) int {
	them := in[0] - 0x40
	strat := in[2] - 0x57

	me := va(them, strat)

	return int(me) + int((strat-1)*3)
}
