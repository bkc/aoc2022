package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	lines := bufio.NewScanner(os.Stdin)

	elves := make(map[int]int64)
	currElf := 0

	for lines.Scan() {
		line := lines.Text()

		if len(line) == 0 {
			currElf++
			continue
		}

		count, err := strconv.ParseInt(line, 10, 64)
		if err != nil {
			panic(err)
		}

		elves[currElf] += count
	}

	max := int64(0)
	holder := 0
	for i, cnt := range elves {
		if cnt > max {
			holder = i
			max = cnt
		}
	}

	fmt.Printf("Part 1: Elf with most is %d with %d\n", holder+1, max)

	keys := make([]int, 0, len(elves))
	for key := range elves {
		keys = append(keys, key)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return elves[keys[i]] > elves[keys[j]]
	})

	tot := int64(0)

	for i, key := range keys {
		if i == 3 {
			break
		}
		tot += elves[key]
	}

	fmt.Printf("Part 2: Top 3 elves carry %d calories\n", tot)

}
